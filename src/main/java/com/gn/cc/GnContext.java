package com.gn.cc;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by GN on 2016/11/27.
 */
public class GnContext {

    /**
     * Storage for key/value pairs.
     */
    private Map<String, String> context = new HashMap<>();

    public GnContext() {

    }

    public GnContext(Map<String, String> context) {
        this.context = context;
    }

    public void put(String key, String value) {
        context.put(key, value);
    }

    public String get(String key) {
        return context.get(key);
    }

}
