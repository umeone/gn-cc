package com.gn.cc;

/**
 * 字符串工具类
 * Created by GN on 2016/11/27.
 */
public abstract class StringUtil {

    /**
     * 判断字符串是否为空
     * @param str
     * @return
     */
    public static boolean hasLength(String str){
        return str != null && str.trim().length() > 0;
    }


}
