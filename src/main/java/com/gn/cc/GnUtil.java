package com.gn.cc;

import java.io.*;

/**
 * Created by GN on 2016/11/27.
 */
public abstract class GnUtil {

    /**
     * 获取模板信息
     * @param filePath 模板路径
     * @return
     */
    public static Template getTemplate(String filePath) throws IOException {
        if (!StringUtil.hasLength(filePath)){
            throw new IllegalArgumentException("模板路径不能为空");
        }
        File file = new File(filePath);
        if (!file.isFile() || !file.exists()){
            throw new IllegalArgumentException("无法找到指定文件:"+filePath);
        }
        return new Template(file);
    }

}
