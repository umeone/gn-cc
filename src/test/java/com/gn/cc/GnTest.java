package com.gn.cc;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Created by GN on 2016/11/27.
 */
public class GnTest {

    @Test
    public void testGn() throws IOException {
        GnContext gnContext = new GnContext();
        gnContext.put("domain","User");
        gnContext.put("lowerDomain","user");
        String path = "E:\\WorkSpace\\gn-cc\\src\\main\\resources\\DaoImpl.java";
        Template template = GnUtil.getTemplate(path);
        String target = "G:\\DaoImpl.java";
        File file = new File(target);
        template.merge(gnContext,file);
    }

}
